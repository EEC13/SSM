package com.zrzy.dao;

import com.zrzy.entity.Employment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by XMH on 2017/3/12.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
@Transactional
@Rollback
public class EmploymentDaoTest {


    @Resource
    private EmploymentDao employmentDao;
    @Test
    public void insert() throws Exception {
        Employment employment=new Employment();
        employment.setStuId(337L);
        employment.setEmpWay("自主就业");
        employment.setAddress("上海");
        employmentDao.insert(employment);

    }

    @Test
    public void update() throws Exception {

    }

    @Test
    public void queryByPrimaryKey() throws Exception {

    }

    @Test
    public void deleteByEmpId() throws Exception {

    }

    @Test
    public void queryGoodnews() throws Exception {
       List<Employment>  map = employmentDao.queryGoodnews();
        System.out.println(map);


    }

    @Test
    public void queryAllGoodnews() throws Exception {
        List<Employment> employments = employmentDao.queryAllGoodnews("android");
        System.out.println(employments.size());

    }
    @Test
    public void queryAllGoodnewsByYear() throws Exception {
        employmentDao.queryAllGoodnewsByYear("2016");
        System.out.println( employmentDao.queryAllGoodnewsByYear("2016"));
    }

}