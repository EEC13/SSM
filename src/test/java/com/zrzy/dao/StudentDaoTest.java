package com.zrzy.dao;

import com.zrzy.entity.Course;
import com.zrzy.entity.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by XMH on 2017/3/10.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
@Transactional
@Rollback

public class StudentDaoTest {
    @Test
    public void queryStudentByName() throws Exception {
        System.out.println( studentDao.queryStudentByName("罗黎军"));
    }

    @Resource
    private StudentDao studentDao;
    @Test
    public void insert() throws Exception {
        Student student=new Student();
        student.setStuName("谢美红");
        student.setEduBackground("本科");
        student.setMajor("通信工程");
        student.setStatus("应届");
       studentDao.insert(student);
    }

    @Test
    public void deleteByStuId() throws Exception {
     studentDao.deleteByStuId(1L);

    }

    @Test
    public void updateByStuId() throws Exception {
        Student student=new Student();
        student.setStuId(1L);
        student.setMajor("计算机");
       studentDao.updateByStuId(student);
    }

    @Test
    public void findById() throws Exception {
        Student student = studentDao.findById(1L);
        System.out.println(student);

    }

}