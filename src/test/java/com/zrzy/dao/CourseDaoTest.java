package com.zrzy.dao;

import com.zrzy.entity.Course;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by XMH on 2017/3/9.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
@Transactional
public class CourseDaoTest {

    @Resource
    private CourseDao courseDao;

    @Test
    public void insert() throws Exception {
        Course course = new Course();
        course.setcName("java中级班");
        course.setCsId(2);
        courseDao.insert(course);
    }

    @Test
    public void deleteById() throws Exception {
        courseDao.deleteById(13L);
    }

    @Test
    public void updateByCId() throws Exception {
        Course course = courseDao.findById(14L);
        course.setBeginsDate(new Date());
        course.setStatus(0);
        course.setCapacity(30);
        course.setCurrentNum(15);
        courseDao.update(course);
    }

    @Test
    public void findByCsId() throws Exception {
        List<Course> byCsId = courseDao.findByCsId(2);
        System.out.println(byCsId);
    }

    @Test
    public void findById() throws Exception {
        Course byId = courseDao.findById(14L);
        System.out.println(byId);
    }

    @Test
    public void queryAsList() throws Exception {
        List<Course> courses = courseDao.queryAsList(0, 5, -1);
        for (Course course : courses) {
            System.out.println(course);
        }
    }

    @Test
    public void getTotalSizeForCourseSys() throws Exception {
        int size = courseDao.getTotalSizeForCourseSys(-1);
        System.out.println(size);
    }

    @Test
    public void deleteByIds() throws Exception {
        Long[] ids = {14L,15L};
        List<Long> longs = Arrays.asList(ids);
        courseDao.deleteByIds(longs);
    }

}