package com.zrzy.dao;

import com.zrzy.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/06 17:17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
@Transactional
@Rollback
public class UserDaoTest {
    //注入 DAO 实现类依赖
    @Resource
    private UserDao userDao;

    @Test
    public void existUsername() throws Exception {
        assertEquals(true, userDao.existUsername("demo2") > 0);
    }

    @Test
    public void updateLoginTime() throws Exception {
        userDao.updateLoginTime(1L);
    }

    @Test
    public void deleteById() throws Exception {

    }

    @Test
    public void findById() throws Exception {

    }

    @Test
    public void login() throws Exception {

    }

    @Test
    public void queryAsList() throws Exception {
        List<User> users = userDao.queryAsList(0, 5);
        System.out.println(users);
    }

    @Test
    public void getAll() throws Exception {

    }

}