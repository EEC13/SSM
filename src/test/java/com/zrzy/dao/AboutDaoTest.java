package com.zrzy.dao;

import com.zrzy.entity.About;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/09 16:04
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
//@Transactional
//@Rollback
public class AboutDaoTest {

    @Resource
    private AboutDao aboutDao;

    @Test
    public void insert() throws Exception {
        About about1 = new About("喜讯：深圳JavaEE首期班毕业两周100%就业 平均薪资12729元", 3, "picture/carousel_01.jpg", null, null);
        About about2 = new About("重磅!蔲丁Python培训耀世起航 首期班钜惠2000还送树莓派", 3, "picture/carousel_02.jpg", null, null);
        About about3 = new About("爱人者 人恒爱之 救救李乐英那患有白血病爸爸的捐款倡议书", 3, "picture/carousel_03.jpg", null, null);
        About about4 = new About("蔲丁课程钜惠来袭 史无前例席卷全国", 3, "picture/carousel_04.jpg", null, null);
        About about5 = new About("喜讯：蔲丁课程钜惠来袭 史无前例席卷全国 平均薪资12729元", 3, "picture/carousel_01.jpg", null, null);
        aboutDao.insert(about1);
        aboutDao.insert(about2);
        aboutDao.insert(about3);
        aboutDao.insert(about4);
        aboutDao.insert(about5);
    }

    @Test
    public void deleteById() throws Exception {
        aboutDao.deleteById(Long.parseLong("8"));
    }

    @Test
    public void updateById() throws Exception {
        About about = new About();
        about.setId(Long.parseLong("7"));
        about.setTitle("women");
        aboutDao.updateById(about);
    }

    @Test
    public void findById() throws Exception {
        About byId = aboutDao.findById(30L);
        System.out.println(byId);
    }

    @Test
    public void findByClassify() throws Exception {
        System.out.println(aboutDao.findByClassify(1));
    }

    @Test
    public void queryAsList() throws Exception {
        List<About> abouts = aboutDao.queryAsList(0, 2);
        System.out.println(abouts);
    }

    @Test
    public void getTotalSize() throws Exception {
        int totalSize = aboutDao.getTotalSize();
        System.out.println(totalSize);
    }

    @Test
    public void deleteByIds() throws Exception {
        Long[] ids = {46L,42L,41L};
        List<Long> longs = Arrays.asList(ids);
        System.out.println(longs);
        aboutDao.deleteByIds(longs);
    }

    @Test
    public void getForCondition() throws Exception {
        List<About> forCondition = aboutDao.getForCondition(2, 4);
        for (About about : forCondition) {
            System.out.println(about);
        }
    }

}