package com.zrzy.dao;

import com.zrzy.entity.Recollection;
import com.zrzy.entity.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lenovo on 2017/3/10.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
public class RecollectionDaoTest {


    @Resource
    private RecollectionDao recollectionDao;

    @Test
    public void queryRecollectionById1() throws Exception {
            recollectionDao.queryRecollectionById(13);
        System.out.println(recollectionDao.queryRecollectionById(13));
    }

    @Test
    public void queryRecollectionByPage () throws  Exception{
       List<Recollection> recollectionList= recollectionDao.queryRecollectionByPage(0,5);
        System.out.println(recollectionList);
    }

   /* @Test
    public void insertRecollection() throws Exception{
        Recollection recollection=new Recollection();
        recollection.setRePhoto("hahahahhaha");
        recollection.setReUil("ehehheheehhe");
        recollection.setReDescription("kekekekekekkeke");
        recollection.getStuId(13L);
        recollectionDao.insertRecollection(recollection*//*,student*//*);
    }*/
    @Test
    public void updateRecollection() throws Exception {
        Recollection recollection = new Recollection();
        recollection.setReId(14);
        recollection.setRePhoto("aaaaa");
        recollection.setReUil("bbbbbbbbbbbbbbbb");
        recollection.setReDescription("ccccccccccccccccc");
        recollection.setStuId(12L);
        recollection.setEmpId(75L);
        recollectionDao.updateRecollection(recollection);
        System.out.println(recollection);
    }
    @Test
    public void deleteRecollectionById() throws Exception {
        recollectionDao.deleteRecollectionById(2);
    }
    @Test
    public void queryRecollectionById() throws Exception {
        Recollection recollections = recollectionDao.queryRecollectionById(3);
        System.out.println(recollections);
    }

    @Test
    public void queryRecollection() throws Exception {
        List<Recollection> recollectionList = recollectionDao.queryRecollection();
        System.out.println(recollectionList);
    }
}