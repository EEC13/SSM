package com.zrzy.dao;

import com.zrzy.entity.CourseSystem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lenovo on 2017/3/9.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
@Transactional
@Rollback
public class CourseSystemDaoTest {

    @Resource
    private CourseSystemDao courseSystemDao;
    /*查*/
    @Test
    public void queryCourseSystem() throws Exception {
        List<CourseSystem> courseSystems = courseSystemDao.queryCourseSystem();
        System.out.println(courseSystems);
    }
    /*增*/
    @Test
    public void insertCourseSystem() throws Exception {
        CourseSystem cs=new CourseSystem();
        cs.setCsName("c++");
        cs.setNickName("niaklj");
        cs.setDescription("sadjbfaljbahfd");
        courseSystemDao.insertCourseSystem(cs);
        System.out.println(cs.toString());
    }
    /*改*/
    @Test
    public void updateCourseSystem() throws Exception {
        CourseSystem cs=new CourseSystem();
        cs.setCsId(1);
        cs.setCsName("yuwen");
        cs.setNickName("anfa");
        cs.setDescription("dajiaaslkfh");
        courseSystemDao.updateCourseSystem(cs);
    }
    /*删*/
    @Test
    public void deleteCourseSystemById() throws Exception {
        courseSystemDao.deleteCourseSystemById(2L);
    }

    @Test
    public void getCourseSystemWithCourse() throws Exception {
        List<CourseSystem> courseSystemWithCourse = courseSystemDao.getCourseSystemWithCourse(-1);
        System.out.println(courseSystemWithCourse);
    }

}