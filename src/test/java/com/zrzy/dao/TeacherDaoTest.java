package com.zrzy.dao;

import com.zrzy.entity.Teacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * Created by lenovo on 2017/3/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
public class TeacherDaoTest {

    @Resource
    private TeacherDao teacherDao;
    /*@Test
    public void queryTeacherByPage() throws Exception {
        System.out.println(teacherDao.queryTeacherByPage());
    }*/
    @Test
    public void insertTeacher() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setTeName("张老师");
        teacher.setTeacherSays("孩子就是我的一切");
        teacher.setTeDescription("张老师的教学实力在我们寇丁教育中是数一数二的");
        teacher.setTeHeader("假装有张老师的照片");
        teacherDao.insertTeacher(teacher);
    }

    @Test
    public void updateTeacher() throws Exception {
        Teacher teacher = new Teacher();
        teacher.setTeId(1);
        teacher.setTeName("李老师");
        teacher.setTeacherSays("一切为了孩子");
        teacher.setTeDescription("李老师是寇丁最为优秀的老师之一");
        teacher.setTeHeader("假装有李老师图片");
        teacherDao.updateTeacher(teacher);
    }
    @Test
    public void deleteTeacher() throws Exception {
        teacherDao.deleteTeacher(2);
    }
    @Test
    public void queryTeacherById() throws Exception {
        System.out.println(teacherDao.queryTeacherById(1));
    }

    @Test
    public void deleteAllTeacher() throws Exception {
        int i = teacherDao.deleteAllTeacher("13,14");
        System.out.println(i);
    }
}