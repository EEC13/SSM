package com.zrzy.dao;

import com.zrzy.entity.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lenovo on 2017/3/10.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/springContext-common.xml"})
@Transactional
@Rollback
public class RoleDaoTest {
    @Resource
    private RoleDao roleDao;
    @Test
    public void queryRole() throws Exception {
    List<Role> listRole=roleDao.queryRole();
        System.out.println(listRole.toString());
    }

    @Test
    public void insertRole() throws Exception {
        Role ro = new Role();
        ro.setRoleName("大娃");
        ro.setDescription("我是大娃，我可以变大哦");
        roleDao.insertRole(ro);
    }

    @Test
    public void updateRole() throws Exception {
            Role role=new Role();
            role.setRoleId(6);
            role.setRoleName("二娃");
            role.setDescription("我是二娃。我有千里眼，顺风耳");
            roleDao.updateRole(role);
    }
    @Test
    public void deleteRoleById() throws Exception {
        roleDao.deleteRoleById(7);
    }

}