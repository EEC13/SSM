package com.zrzy.entity;

import java.util.Date;

/**
 * 就业信息表
 *
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/07 17:06
 */
public class Employment {
    /**
     * 受雇 ID
     */
    private Long empId;

    /**
     * 学员 ID
     */
    private Long stuId;

    private Student stu;

    /**
     * 入职企业
     */
    private String enterprise;

    /**
     * 月薪
     */
    private Double monSalary;

    /**
     * 地点
     */
    private String address;

    /**
     * 受雇日期
     */
    private Date hireDate;

    /**
     * 就业方式
     */
    private String empWay;

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public Double getMonSalary() {
        return monSalary;
    }

    public void setMonSalary(Double monSalary) {
        this.monSalary = monSalary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public String getEmpWay() {
        return empWay;
    }

    public void setEmpWay(String empWay) {
        this.empWay = empWay;
    }

    public Long getStuId() {
        return stuId;
    }

    public void setStuId(Long stuId) {
        this.stuId = stuId;
    }

    public Student getStu() {
        return stu;
    }

    public void setStu(Student stu) {
        this.stu = stu;
    }

    @Override
    public String toString() {
        return "Employment{" +
                "empId=" + empId +
                ", stu=" + stu +
                ", enterprise='" + enterprise + '\'' +
                ", monSalary=" + monSalary +
                ", address='" + address + '\'' +
                ", hireDate=" + hireDate +
                ", empWay='" + empWay + '\'' +
                '}';
    }
}
