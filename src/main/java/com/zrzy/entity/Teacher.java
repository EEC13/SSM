package com.zrzy.entity;

/**
 * 教师团队表
 * Created by lenovo on 2017/3/14.
 */
public class Teacher {
    /**
     * 教师ID
     */
    private Integer teId;
    /**
     * 教师姓名
     */
    private String teName;
    /**
     * 讲师语录
     */
    private String teacherSays;
    /**
     * 教师描述
     */
    private String teDescription;
    /**
     * 教师头像
     */
    private String teHeader;

    public Integer getTeId() {
        return teId;
    }

    public void setTeId(Integer teId) {
        this.teId = teId;
    }

    public String getTeName() {
        return teName;
    }

    public void setTeName(String teName) {
        this.teName = teName;
    }

    public String getTeacherSays() {
        return teacherSays;
    }

    public void setTeacherSays(String teacherSays) {
        this.teacherSays = teacherSays;
    }

    public String getTeDescription() {
        return teDescription;
    }

    public void setTeDescription(String teDescription) {
        this.teDescription = teDescription;
    }

    public String getTeHeader() {
        return teHeader;
    }

    public void setTeHeader(String teHeader) {
        this.teHeader = teHeader;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teId=" + teId +
                ", teName='" + teName + '\'' +
                ", teacherSays='" + teacherSays + '\'' +
                ", teDescription='" + teDescription + '\'' +
                ", teHeader='" + teHeader + '\'' +
                '}';
    }
}
