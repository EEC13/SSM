package com.zrzy.entity;

/**
 * 学员信息表
 *
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/07 16:55
 */
public class Student {
    /**
     * 学员 ID
     */
    private Long stuId;

    /**
     * 学员姓名
     */
    private String stuName;

    /**
     * 学历
     */
    private String eduBackground;

    /**
     * 目前状态
     */
    private String status;

    /**
     * 专业
     */
    private String major;

    /**
     * 图像
     */
    private String photo;

    /**
     * 课程 ID
     */
    private Long courseId;
    private Course course;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Long getStuId() {
        return stuId;
    }

    public void setStuId(Long stuId) {
        this.stuId = stuId;
    }

    public String getStuName() {
        return stuName;
    }

    public void setStuName(String stuName) {
        this.stuName = stuName;
    }

    public String getEduBackground() {
        return eduBackground;
    }

    public void setEduBackground(String eduBackground) {
        this.eduBackground = eduBackground;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

   public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "stuId=" + stuId +
                ", stuName='" + stuName + '\'' +
                ", eduBackground='" + eduBackground + '\'' +
                ", status='" + status + '\'' +
                ", major='" + major + '\'' +
                ", photo='" + photo + '\'' +
                ", course=" + course +
                '}';
    }
}
