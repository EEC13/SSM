package com.zrzy.entity;

/**
 * 学生感言表
 * Author:  Xue Mengju
 * Created by lenovo on 2017/3/10.
 */
public class Recollection {
    /**
     *学生感言表ID
     */
    private Integer reId;
    /**
     * 学生感言图像
     */
    private String rePhoto;
    /**
     * 视频连接地址
     */
    private String reUil;
    /**
     * 描述
     */
     private String reDescription;
    /**
     * 学生表ID
     */
    private Long stuId;
    private Student student;

    private Course course;
     /**
     *就业表ID
     */
    private Long empId;

    private Employment employment;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Employment getEmployment() {
        return employment;
    }

    public void setEmployment(Employment employment) {
        this.employment = employment;
    }



    public Integer getReId() {
        return reId;
    }

    public void setReId(Integer reId) {
        this.reId = reId;
    }

    public String getRePhoto() {
        return rePhoto;
    }

    public void setRePhoto(String rePhoto) {
        this.rePhoto = rePhoto;
    }

    public String getReUil() {
        return reUil;
    }

    public void setReUil(String reUil) {
        this.reUil = reUil;
    }

    public String getReDescription() {
        return reDescription;
    }

    public void setReDescription(String reDescription) {
        this.reDescription = reDescription;
    }

    public Long getStuId() {
        return stuId;
    }

    public void setStuId(Long stuId) {
        this.stuId = stuId;
    }

    public Long getEmpId() {
        return empId;
    }

    public void setEmpId(Long empId) {
        this.empId = empId;
    }

    @Override
    public String toString() {
        return "Recollection{" +
                "reId=" + reId +
                ", rePhoto='" + rePhoto + '\'' +
                ", reUil='" + reUil + '\'' +
                ", reDescription='" + reDescription + '\'' +
                ", stuId=" + stuId +
                ", student=" + student +
                ", course=" + course +
                ", empId=" + empId +
                ", employment=" + employment +
                '}';
    }
}
