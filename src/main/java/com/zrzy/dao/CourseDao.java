package com.zrzy.dao;

import com.zrzy.entity.Course;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseDao {
    /*插入一条课程信息*/
    int insert(Course course);

    /*根据id删除一条课程信息*/
    int deleteById(Long cId);

    /*更新课程信息*/
    int update(Course course);

    /*根据课程体系csId查询课程信息*/
    List<Course> findByCsId(Integer csId);

    /**
     * 根据 ID 查询
     *
     * @param id ID
     * @return 实体对象
     */
    Course findById(@Param("id") Long id);

    /**
     * 分页查询(所有记录)当前页码的记录
     *
     * @param offset 偏移量
     * @param size   偏移大小
     * @return 实体对象集合
     */
    List<Course> queryAsList(@Param("offset") int offset, @Param("size") int size, @Param("csId") Integer courseSysId);

    /**
     * 查询对应课程体系下课程数
     *
     * @return 记录条数
     */
    int getTotalSizeForCourseSys(@Param("csId") Integer courseSysId);

    /**
     * 根据 ID 集合删除多条记录
     *
     * @param ids ID 集合
     */
    int deleteByIds(List<Long> ids);


}
