package com.zrzy.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.zrzy.entity.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao {
    /*查*/
    List<Role> queryRole();
    /*增*/
    int insertRole(Role param);
    /*更*/
    int updateRole(Role role);
    /*删*/
    int deleteRoleById(Integer csId);
}
