package com.zrzy.dao;

import com.zrzy.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.zrzy.entity.Recollection;
import org.springframework.stereotype.Repository;

@Repository
public interface RecollectionDao {
    /**
     * 分页查询(所有记录)当前页码的记录
     *
     * @param offset 偏移量
     * @param size   偏移大小
     * @return 实体对象集合
     */
    /*查询感言表所有信息*/
    List<Recollection> queryRecollectionByPage(@Param("offset") int offset, @Param("size") int size);

    List<Recollection> queryRecollection();
    /**
     * 对应查询条件总记录条数
     *
     * @return 记录条数
     */
    int getTotalSize();
    /*增加信息*/
    int insertRecollection(Recollection recollection);
    /*更改信息*/
    int updateRecollection(Recollection recollection);
    /*删除信息*/
    int deleteRecollectionById(Integer id);
    /*通过ID查询所有信息*/
    Recollection queryRecollectionById(Integer id);
    /*通过多个ID删除多条信息*/
    int deleteRecollectionByIds(@Param("ids") String ids);
}
