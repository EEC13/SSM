package com.zrzy.dao;


import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.zrzy.entity.Teacher;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherDao {
    /*查询*/
    List<Teacher> queryTeacherByPage(@Param("offset") int offset, @Param("size") int size);
    /*查询总数*/
    int getTotalSize();
    /*通过ID查询*/
    Teacher queryTeacherById(Integer id);
    /*插入*/
    int insertTeacher(Teacher teacher);
    /*更新*/
    int updateTeacher(Teacher teacher);
    /*删除*/
    int deleteTeacher(Integer id);
    /*删除多个*/
    int deleteAllTeacher(@Param("teId") String ids);
}
