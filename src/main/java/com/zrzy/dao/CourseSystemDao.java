package com.zrzy.dao;


import com.zrzy.entity.CourseSystem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseSystemDao {
    //查
    List<CourseSystem> queryCourseSystem();

    //增
    int insertCourseSystem(CourseSystem courseSystem);

    //更
    int updateCourseSystem(CourseSystem courseSystem);

    //删
    int deleteCourseSystemById(@Param("csId") Long csId);

    /**
     * 对应查询条件总记录条数
     *
     * @return 记录条数
     */
    int getTotalSize();

    /**
     * 分页查询(所有记录)当前页码的记录
     *
     * @param offset 偏移量
     * @param size   偏移大小
     * @return 实体对象集合
     */
    List<CourseSystem> queryAsList(@Param("offset") int offset, @Param("size") int size);


    /**
     * 根据 ID 查询
     *
     * @param csId ID
     * @return 实体对象
     */
    CourseSystem findById(@Param("csId") Long csId);

    /**
     * 查询课程体系信息并携带课程信息
     *
     * @param courseId 课程体系 ID
     * @return 集合列表
     */
    List<CourseSystem> getCourseSystemWithCourse(@Param("courseId") Integer courseId);
}
