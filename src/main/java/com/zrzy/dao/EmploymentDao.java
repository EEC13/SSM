package com.zrzy.dao;

import com.zrzy.entity.Employment;
import com.zrzy.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface EmploymentDao {
    //增
    int insert(Employment employment);

    //更新
    int update(Employment employment);

    //查询
    Employment queryByPrimaryKey(Long empId);

    //删除
    int deleteByEmpId(Long empId);

    //根据id删除多个
    int deleteEmpByIds(@Param("ids") String ids);

    //联表查询所有喜报
    List<Employment> queryGoodnews();

    //分页查询
    List<Employment> queryGoodnewsByPage(@Param("o") int offset, @Param("s") int size);

    //根据课程体系查询所有喜报
    List<Employment> queryAllGoodnews(@Param("str") String csname);

    /*根据年份，或者月份查询所有喜报*/
    List<Employment> queryAllGoodnewsByYear(@Param("year") String year);

}
