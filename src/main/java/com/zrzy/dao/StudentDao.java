package com.zrzy.dao;

import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.zrzy.entity.Student;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDao {
   int insert(Student student);
   int deleteByStuId(Long stuId);
   int updateByStuId(Student student);
   Student findById(Long id);
   /*根据姓名查询学生*/
   Student queryStudentByName(String stuName);

}
