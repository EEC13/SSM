package com.zrzy.dao;

import com.zrzy.entity.About;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AboutDao {
    /*插入一条新闻*/
    int insert(About about);

    /*根据id删除一条新闻*/
    int deleteById(Long id);

    /*根据id更新一条新闻*/
    int updateById(About about);

    /*根据分类查询新闻*/
    List<About> findByClassify(Integer classify);

    /**
     * 根据 ID 查询
     *
     * @param id ID
     * @return 实体对象
     */
    About findById(@Param("id") Long id);

    /**
     * 分页查询(所有记录)当前页码的记录
     *
     * @param offset 偏移量
     * @param size   偏移大小
     * @return 实体对象集合
     */
    List<About> queryAsList(@Param("offset") int offset, @Param("size") int size);

    /**
     * 对应查询条件总记录条数
     *
     * @return 记录条数
     */
    int getTotalSize();

    /**
     * 根据集合 ID 删除多条记录
     *
     * @param aboutIds  ID 集合
     */
    int deleteByIds(List<Long> aboutIds);


    /**
     * 获取指定分类的指定记录数的最新记录
     *
     * @param classify 分类
     * @param length   记录数
     * @return 对象集合
     */
    List<About> getForCondition(@Param("classify") Integer classify,@Param("length") Integer length);
}
