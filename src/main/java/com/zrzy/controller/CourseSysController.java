package com.zrzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.sun.tools.internal.ws.processor.model.Model;
import com.zrzy.beans.PageBean;
import com.zrzy.entity.Course;
import com.zrzy.entity.CourseSystem;
import com.zrzy.service.CourseSystemService;
import org.apache.ibatis.annotations.ResultMap;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by gtz on 2017/3/12.
 */
@Controller
@RequestMapping("admin/course_sys")
public class CourseSysController {
    @Resource
    private CourseSystemService courseSystemService;

    /**
     * course_sys 页面
     *
     * @return 模板文件
     */
    @RequestMapping("courseSysList.html")
    public ModelAndView findForPage(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                    @RequestParam(value = "s", required = false, defaultValue = "10") int pageSize) {
        ModelAndView mv = new ModelAndView("admin/list_course_sys");
        //课程体系数据
        PageBean<CourseSystem> courseSystemPageBean = courseSystemService.queryForPage(pageNumber, pageSize);
        mv.addObject("courseSystemPageBean", courseSystemPageBean);
        return mv;
    }

    /**
     * CourseSystem 编辑（增加、修改）页面
     *
     * @return 模板文件
     */
    @RequestMapping("edit.html")
    public ModelAndView editCourseSys(@ModelAttribute("item") CourseSystem courseSystem,
                                      @RequestParam(value = "id", required = false, defaultValue = "-1") Long csId) {
        ModelAndView mv = new ModelAndView("admin/edit_course_sys");
        CourseSystem tmp = courseSystemService.findById(csId);
        if (csId != -1 && null != tmp) {
            courseSystem.setCsId(tmp.getCsId());
            courseSystem.setCsName(tmp.getCsName());
            courseSystem.setNickName(tmp.getNickName());
            courseSystem.setDescription(tmp.getDescription());
        }
        return mv;
    }

    /**
     * About 编辑（增加、修改）动作
     *
     * @param courseSystem 实体对象
     */
    @RequestMapping(value = "edit")
    public String saveAbout(CourseSystem courseSystem,
                            @RequestParam(value = "csId", required = false, defaultValue = "-1") Long csId) {
        if (csId != -1) {
            courseSystemService.updateCourseSystem(courseSystem);
        } else {
            courseSystemService.insertCourseSystem(courseSystem);
        }
        return "redirect:courseSysList.html";
    }

    /**
     * 根据 ID 删除 CourseSystem 记录
     *
     * @param csId 记录主键 ID
     * @return JSON 数据
     */
    @RequestMapping(value = "delete/{csId}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteById(@PathVariable Long csId) {
        System.out.println(csId);
        JSONObject result = new JSONObject();
        int i = courseSystemService.deleteCourseSystemById(csId);
        result.put("status", i);
        return result;
    }
}
