package com.zrzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.zrzy.beans.PageBean;
import com.zrzy.entity.Teacher;
import com.zrzy.service.TeacherService;
import com.zrzy.utils.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * Created by lenovo on 2017/3/14.
 */
@Controller
@RequestMapping("admin")
public class TeacherController {
    @Resource
    private TeacherService teacherService;
    @RequestMapping("teacher/tlist.html")
    public ModelAndView listTeacher(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                    @RequestParam(value = "s", required = false, defaultValue = "5") int pageSize){
        ModelAndView mv=new ModelAndView("admin/list_teacher");
        PageBean<Teacher> teacherPageBean=teacherService.queryForPage(pageNumber,pageSize);
        mv.addObject("teacherPageBean",teacherPageBean);
        return mv;
    }
    @RequestMapping("teacher/Add.html")
    public ModelAndView dropToAdd() {
        ModelAndView mv = new ModelAndView("admin/edit_teacher");
        return mv;
    }
    /*删除单个信息*/
    @RequestMapping(value = "teacher/delete/{id}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteTeacher(@PathVariable Integer id) {
        int delete = teacherService.deleteTeacher(id);
        JSONObject result = new JSONObject();
        result.put("status", delete);
        return result;
    }
    /*删除多条记录*/
    @RequestMapping(value = "teacher/deletes/{ids}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteAllTeacher(@PathVariable String ids) {
        int delete = teacherService.deleteAllTeacher(ids);
        JSONObject results = new JSONObject();
        results.put("status", delete);
        return results;
    }
    @RequestMapping("teacher/insetTeacher.html")
    public ModelAndView addTeacherUrl(@ModelAttribute("item") Teacher teacher,
                                      @RequestParam(value = "teid",required = false,defaultValue = "-1") Integer teid){
        ModelAndView mv = new ModelAndView("admin/edit_teacher");
        Teacher te=teacherService.queryTeacherById(teid);
        if(teid!=-1 && te!=null){
            teacher.setTeId(te.getTeId());
            teacher.setTeName(te.getTeName());
            teacher.setTeacherSays(te.getTeacherSays());
            teacher.setTeDescription(te.getTeDescription());
            teacher.setTeHeader(te.getTeHeader());
        }
        return mv;
    }

    /*插入讲师信息*/
    @RequestMapping("teacher/insertTeacher")
    public String saveTeacher(Teacher teacher, MultipartFile file,HttpServletRequest request,
                              @RequestParam(value = "teId",required = false,defaultValue = "-1") Integer teid){
        StringBuilder destLocation = new StringBuilder("upload/teacherPicture");
        // 利用request对象得到项目的realPath
        String path = request.getSession().getServletContext().getRealPath(destLocation.toString());
        // 利用工具类生成唯一文件名称
        if (!file.isEmpty()) {
            String destFileName = CommonUtils.UUIDName(file.getOriginalFilename());
            File destFile = new File(path, destFileName);
            if (!destFile.getParentFile().exists()) {
                // 判断父类文件夹存不存在，不存在创建一个
                destFile.mkdirs();
            }
            try {
                file.transferTo(destFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            teacher.setTeHeader(destLocation.append("/").append(destFileName).toString());
        }
        if (teid!=-1){
            teacherService.updateTeacher(teacher);
        }else{
            teacherService.insertTeacher(teacher);
        }
        return "redirect:tlist.html";
    }
}
