package com.zrzy.controller;

import com.zrzy.beans.PageBean;
import com.zrzy.entity.*;
import com.zrzy.entity.About;
import com.zrzy.entity.Recollection;
import com.zrzy.entity.Teacher;
import com.zrzy.service.AboutService;
import com.zrzy.service.CourseSystemService;
import com.zrzy.service.EmploymentService;
import com.zrzy.service.RecollectionService;
import com.zrzy.service.TeacherService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author:  Wu Yujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/07 21:41
 */
@Controller
@RequestMapping(path = "/")
public class IndexController {

    @Resource
    private AboutService aboutService;

    @Resource
    private RecollectionService recollectionService;

    @Resource
    private TeacherService teacherService;

    @Resource
    private EmploymentService employmentService;

    @Resource
    private CourseSystemService courseSystemService;

    /**
     * 网站主界面
     *
     * @return 模板文件名
     */
    @RequestMapping(path = {"/", "index.html"})
    public ModelAndView indexPage() {
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("title", "首页");

        //头部轮播数据
        List<About> carouse = aboutService.findByClassify(0);
        mv.addObject("carouse", carouse);

        List<CourseSystem> carouseSys = courseSystemService.queryCourseSystem();
        mv.addObject("carouseSys", carouseSys);

        //课程信息
        List<CourseSystem> carouseInfo = courseSystemService.getCourseSystemWithCourse(-1);
        mv.addObject("carouseInfo", carouseInfo);

        //蔲丁动态
        List<About> news = aboutService.getForCondition(1,4);
        mv.addObject("news", news);

        //蔲丁故事
        List<About> story = aboutService.getForCondition(2,4);
        mv.addObject("story", story);

        //行业动态
        List<About> dynamic = aboutService.getForCondition(3,4);
        mv.addObject("dynamic", dynamic);

        //就业数据
        List<Employment> employments = employmentService.queryGoodnews();
        mv.addObject("employment", employments);

        return mv;
    }

    /**
     * 公用头部
     *
     * @return 模板文件名
     */
    @RequestMapping("header")
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView("common/header");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 公用底部
     *
     * @return 模板文件名
     */
    @RequestMapping("footer")
    public ModelAndView footer() {
        ModelAndView mv = new ModelAndView("common/footer");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * Android 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("course/android.html")
    public ModelAndView ad() {
        ModelAndView mv = new ModelAndView("course/android");
        List<Employment> ad = employmentService.queryAllGoodnews("android");
        for (Employment employment : ad) {
            Student stu = employment.getStu();
            String name = stu.getStuName();
            String newname = name.replace(name.charAt(1), '*');
            stu.setStuName(newname);
            employment.setStu(stu);
        }
        mv.addObject("adlist", ad);
        return mv;
    }
    /**
     * h5 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("course/html5.html")
    public ModelAndView htm() {
        ModelAndView mv = new ModelAndView("course/html5");
        List<Employment> htemp=employmentService.queryAllGoodnews("html5");
        for (Employment employment: htemp ) {
            Student stu=employment.getStu();
            String name=stu.getStuName();
            String newname=name.replace(name.charAt(1),'*');
            stu.setStuName(newname);
            employment.setStu(stu);
        }
        //System.out.println(htemp);
        mv.addObject("htlist", htemp);
        return mv;
    }
    /**
     * UI 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("course/ui.html")
    public ModelAndView ui() {
        ModelAndView mv = new ModelAndView("course/ui");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
    /**
     * php 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("course/php.html")
    public ModelAndView php() {
        ModelAndView mv = new ModelAndView("course/php");
       List<Employment> phplist=employmentService.queryAllGoodnews("php");
        for (Employment employment: phplist ) {
            Student stu=employment.getStu();
            String name=stu.getStuName();
            String newname=name.replace(name.charAt(1),'*');
            stu.setStuName(newname);
            employment.setStu(stu);
        }
        mv.addObject("phplist", phplist);
        return mv;
    }
    /**
     * java 主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("course/java.html")
    public ModelAndView java() {
        ModelAndView mv = new ModelAndView("course/java");
       List<Employment> javalist=employmentService.queryAllGoodnews("java");
//        for (Employment employment: javalist ) {
//            Student stu=employment.getStu();
//            String name=stu.getStuName();
//            String newname=name.replace(name.charAt(1),'*');
//            stu.setStuName(newname);
//            employment.setStu(stu);
//        }
//        mv.addObject("javalist", javalist);
        return mv;
    }
    /**
     * 大数据主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("course/big_data.html")
    public ModelAndView big() {
        ModelAndView mv = new ModelAndView("course/big_data");
      List<Employment> bigdatalist= employmentService.queryAllGoodnews("big_data");
        for (Employment employment: bigdatalist ) {
            Student stu=employment.getStu();
            String name=stu.getStuName();
            String newname=name.replace(name.charAt(1),'*');
            stu.setStuName(newname);
            employment.setStu(stu);
        }
        mv.addObject("bigdatalist", bigdatalist);
        return mv;
    }

    /**
     * 讲师团队主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("team/teacher.html")
    public ModelAndView teachTeam(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                  @RequestParam(value = "s", required = false, defaultValue = "6") int pageSize) {
        ModelAndView mv = new ModelAndView("teacher");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        PageBean<Teacher> teacherPageBean=teacherService.queryForPage(pageNumber,pageSize);
        mv.addObject("teacherPageBean",teacherPageBean);
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 就业喜报
     *
     * @return 模板文件名
     */
    @RequestMapping("employment/carmakers.html")
    public ModelAndView employmentCarmakers() {
        ModelAndView mv = new ModelAndView("employment");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        List<Employment> employmentList=employmentService.queryGoodnews();
        mv.addObject("employmentList",employmentList);
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 学生感言主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("student/testimonials.html")
    public ModelAndView testimonials(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                     @RequestParam(value = "s", required = false, defaultValue = "6") int pageSize) {
        ModelAndView mv = new ModelAndView("testimonials");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        PageBean<Recollection> recollectionPageBean = recollectionService.queryForPage(pageNumber, pageSize);
        mv.addObject("recollectionPageBean", recollectionPageBean);
        mv.addObject("title", "Coding……");
        return mv;
    }

    /**
     * 关于蔲丁主界面
     *
     * @return 模板文件名
     */
    @RequestMapping("about/coding.html")
    public ModelAndView aboutCoding() {
        ModelAndView mv = new ModelAndView("about_coding");
        // TODO: 2017/3/7 0007 22:16 添加业务数据
        mv.addObject("title", "Coding……");
        return mv;
    }
}
