package com.zrzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.zrzy.beans.PageBean;
import com.zrzy.entity.About;
import com.zrzy.service.AboutService;
import com.zrzy.utils.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;

/**
 * 关于蔻丁
 * Created with IntelliJ IDEA.
 * Author:  WuYujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/12 13:36
 */
@Controller
@RequestMapping("admin/about")
public class AboutController {
    @Resource
    private AboutService aboutService;

    /**
     * About 列表页面
     *
     * @return 模板文件
     */
    @RequestMapping("list.html")
    public ModelAndView findForPage(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                    @RequestParam(value = "s", required = false, defaultValue = "10") int pageSize) {
        ModelAndView mv = new ModelAndView("admin/list_about");
        //头部轮播数据
        PageBean<About> aboutPageBean = aboutService.queryForPage(pageNumber, pageSize);
        mv.addObject("aboutPageBean", aboutPageBean);
        return mv;
    }

    /**
     * About 编辑（增加、修改）页面
     *
     * @return 模板文件
     */
    @RequestMapping("edit.html")
    public ModelAndView addAboutUrl(@ModelAttribute("item") About about,
                                    @RequestParam(value = "id", required = false, defaultValue = "-1") Long id) {
        ModelAndView mv = new ModelAndView("admin/edit_about");
        About tmp = aboutService.findById(id);
        if (id != -1 && null != tmp) {
            about.setId(tmp.getId());
            about.setTitle(tmp.getTitle());
            about.setClassify(tmp.getClassify());
            about.setContent(tmp.getContent());
            about.setDescription(tmp.getDescription());
        }
        return mv;
    }


    /**
     * 根据 ID 删除 About 记录
     *
     * @param id 记录主键 ID
     * @return JSON 数据
     */
    @RequestMapping(value = "delete/{id}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteById(@PathVariable Long id) {
        JSONObject result = new JSONObject();
        int i = aboutService.deleteById(id);
        result.put("status", i);
        return result;
    }



    /**
     * About 编辑（增加、修改）动作
     *
     * @param about   实体对象
     * @param file    封面图像
     * @param request 请求
     */
    @RequestMapping(value = "edit")
    public String saveAbout(About about, MultipartFile file, HttpServletRequest request,
                            @RequestParam(value = "id", required = false, defaultValue = "-1") Long id) {
        // 上传文件的位置
        StringBuilder destLocation = new StringBuilder("upload/about");

        // 利用request对象得到项目的realPath
        String path = request.getSession().getServletContext().getRealPath(destLocation.toString());

        // 利用工具类生成唯一文件名称
        if (!file.isEmpty()) {
            String destFileName = CommonUtils.UUIDName(file.getOriginalFilename());
            System.err.println(file.getOriginalFilename());
            File destFile = new File(path, destFileName);
            if (!destFile.getParentFile().exists()) {
                // 判断父类文件夹存不存在，不存在创建一个
                destFile.mkdirs();
            }
            try {
                file.transferTo(destFile);
            } catch (Exception e) {
                e.printStackTrace();
            }


            // 设置存入数据库中图像资源的文件路径名称
            about.setImage(destLocation.append("/").append(destFileName).toString());
        }
        if (id != -1) {
            aboutService.update(about);
        } else {
            aboutService.insert(about);
        }
        return "redirect:list.html";
    }

}
