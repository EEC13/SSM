package com.zrzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.zrzy.beans.PageBean;
import com.zrzy.entity.Course;
import com.zrzy.entity.CourseSystem;
import com.zrzy.service.CourseService;
import com.zrzy.service.CourseSystemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 关于蔻丁
 * Created with IntelliJ IDEA.
 * Author:  WuYujie
 * Email:  coffee377@dingtalk.com
 * Time:  2017/03/12 13:36
 */
@Controller
@RequestMapping("admin/course")
public class CourseController {
    @Resource
    private CourseService courseService;

    @Resource
    private CourseSystemService courseSystemService;

    /**
     * Course 列表页面
     *
     * @return 模板文件
     */
    @RequestMapping("list.html")
    public ModelAndView findForPage(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                    @RequestParam(value = "s", required = false, defaultValue = "10") int pageSize,
                                    @RequestParam(value = "t", required = false, defaultValue = "-1") int courseSysId) {
        ModelAndView mv = new ModelAndView("admin/list_course");
        PageBean<Course> aboutPageBean = courseService.queryForPage(pageNumber, pageSize, courseSysId);
        mv.addObject("aboutPageBean", aboutPageBean);
        List<CourseSystem> courseSystems = courseSystemService.queryCourseSystem();
        mv.addObject("courseSys", courseSystems);
        return mv;
    }

    /**
     * About 编辑（增加、修改）页面
     *
     * @return 模板文件
     */
    @RequestMapping("edit.html")
    public ModelAndView editCourseUrl(@ModelAttribute("item") Course course,
                                    @RequestParam(value = "cId", required = false, defaultValue = "-1") Long id) {
        ModelAndView mv = new ModelAndView("admin/edit_course");
        Course tmp = courseService.findById(id);
        List<CourseSystem> courseSystems = courseSystemService.queryCourseSystem();
        mv.addObject("courseSys", courseSystems);
        if (id != -1 && null != tmp) {
            course.setcId(tmp.getcId());
            course.setCsId(tmp.getCsId());
            course.setcName(tmp.getcName());
            course.setBeginsDate(tmp.getBeginsDate());
            course.setStatus(tmp.getStatus());
            course.setCapacity(tmp.getCapacity());
            course.setCurrentNum(tmp.getCurrentNum());
        }
        return mv;
    }


    /**
     * 根据 ID 删除 course 记录
     *
     * @param id 记录主键 ID
     * @return JSON 数据
     */
    @RequestMapping(value = "delete/{id}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteById(@PathVariable Long id) {
        JSONObject result = new JSONObject();
        int i = courseService.deleteById(id);
        result.put("status", i);
        return result;
    }


    /**
     * About 编辑（增加、修改）动作
     *
     * @param course 实体对象
     */
    @RequestMapping(value = "edit")
    public String saveCourse(Course course, String time, @RequestParam(value = "cId", required = false, defaultValue = "-1") Long id) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(time);
            course.setBeginsDate(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (id != -1) {
            System.err.println(course);
            courseService.update(course);
        } else {
            courseService.insert(course);
        }
        return "redirect:list.html";
    }

}
