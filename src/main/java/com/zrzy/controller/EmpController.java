package com.zrzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.zrzy.beans.PageBean;
import com.zrzy.entity.Employment;
import com.zrzy.service.EmploymentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by XMH on 2017/3/12.
 */
@Controller
@RequestMapping("admin")
public class EmpController {
    @Resource
    private EmploymentService employmentService;

    //分页查询就业信息
    @RequestMapping("employ/list.html")
    public ModelAndView listEmpMsg(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                   @RequestParam(value = "s", required = false, defaultValue = "10") int pageSize) {
        ModelAndView mv = new ModelAndView();
//        List<Employment> employmentList=employmentService.queryGoodnewsByPage(pageNumber,pageSize);
        PageBean<Employment> employmentPageBean = employmentService.queryForPage(pageNumber, pageSize);
        mv.addObject("empPageBean", employmentPageBean);
        mv.setViewName("admin/listEmpMsg");
        return mv;
    }

    //用于跳转到就业页面
    @RequestMapping("employ/add.html")
    public ModelAndView toAddView(@ModelAttribute("item") Employment employment,
                                  @RequestParam(value = "id", required = false, defaultValue = "-1") Long id) {
        ModelAndView mv = new ModelAndView("admin/edit_emp");
        Employment emp = employmentService.queryByPrimaryKey(id);
        if (id != -1 && null != emp) {
            employment.setEmpId(emp.getEmpId());
            employment.setStuId(emp.getStuId());
            employment.setEnterprise(emp.getEnterprise());
            employment.setMonSalary(emp.getMonSalary());
            employment.setAddress(emp.getAddress());
            employment.setHireDate(emp.getHireDate());
            employment.setEmpWay(emp.getEmpWay());
        }
        return mv;
    }

    @RequestMapping("employ/save")
    public String saveEmp(Employment employment, String times, @RequestParam(value = "empId", required = false, defaultValue = "-1") Long id) {
        System.out.println(times);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(times);
            employment.setHireDate(date);
//            System.out.println("sss");
//            System.out.println(employment);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (id != -1) {
            employmentService.update(employment);
        }else{
            employmentService.insert(employment);
        }
        return "redirect:list.html";
    }

    //删除单个就业记录
    @RequestMapping(value = "employ/delete/{id}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteEmp(@PathVariable Long id) {
        JSONObject result = new JSONObject();
        int code = employmentService.deleteByEmpId(id);
        result.put("status", code);
        return result;
    }

    @RequestMapping(value = "employ/deletemany/{ids}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteEmps(@PathVariable String ids) {
        JSONObject result = new JSONObject();
        int codes = employmentService.deleteEmpByIds(ids);
        result.put("status", codes);
        return result;
    }

}
