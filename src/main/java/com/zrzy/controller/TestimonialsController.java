package com.zrzy.controller;

import com.alibaba.fastjson.JSONObject;
import com.zrzy.beans.PageBean;
import com.zrzy.entity.Recollection;
import com.zrzy.service.RecollectionService;
import com.zrzy.service.StudentService;
import com.zrzy.utils.CommonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.annotation.Resource;
import java.io.File;

/**
 * Created by lenovo on 2017/3/12.
 */
@Controller
@RequestMapping("admin")
public class TestimonialsController {
    private Recollection recollection;
    @Resource
    private RecollectionService recollectionService;
    @Resource
    private StudentService studentService;

    /**
     * Testimonials 编辑页面
     * 查询所有信息
     */
    @RequestMapping("testimonials/test.html")
    public ModelAndView Testimonials(@RequestParam(value = "p", required = false, defaultValue = "1") int pageNumber,
                                     @RequestParam(value = "s", required = false, defaultValue = "5") int pageSize) {
        ModelAndView mv = new ModelAndView("admin/list_testimonials");
        PageBean<Recollection> recollectionPageBean = recollectionService.queryForPage(pageNumber, pageSize);
        mv.addObject("recollectionPageBean", recollectionPageBean);
        return mv;
    }

    @RequestMapping("testimonials/dropAdd.html")
    public ModelAndView dropToAdd() {
        ModelAndView mv = new ModelAndView("admin/addTestimonials");
        return mv;
    }

    /*删除单个信息*/
    @RequestMapping(value = "testimonials/delete/{id}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteTestimonials(@PathVariable Integer id) {
        int delete = recollectionService.deleteRecollectionById(id);
        JSONObject result = new JSONObject();
        result.put("status", delete);
        return result;
    }
    /*删除多条记录*/
    @RequestMapping(value = "testimonials/deletes/{ids}", produces = "application/json")
    @ResponseBody
    public JSONObject deleteAllTestimonials(@PathVariable String ids) {
        int delete = recollectionService.deleteRecollectionByIds(ids);
        JSONObject results = new JSONObject();
        results.put("status", delete);
        return results;
    }
    @RequestMapping("testimonials/insettest.html")
    public ModelAndView addRecollectionUrl(@ModelAttribute("item") Recollection recollection,
                                    @RequestParam(value = "reid",required = false,defaultValue = "-1") Integer reid) {
        ModelAndView mv = new ModelAndView("admin/addTestimonials");
        Recollection re=recollectionService.queryRecollectionById(reid);
        if(reid != -1 && null != re ) {
            recollection.setReId(re.getReId());
            recollection.setReUil(re.getReUil());
            recollection.setRePhoto(re.getRePhoto());
            recollection.setReDescription(re.getReDescription());
            recollection.setStuId(re.getStuId());
            recollection.setEmpId(re.getEmpId());
        }
        return mv;
    }
    /*插入信息*/
    @RequestMapping("testimonials/insettest")
    public String saveTestimonials(Recollection recollection,HttpServletRequest request, MultipartFile file,
                                    @RequestParam(value = "reId",required = false,defaultValue = "-1") Integer reid){
        StringBuilder destLocation = new StringBuilder("upload/vidoPicture");
        // 利用request对象得到项目的realPath
        String path = request.getSession().getServletContext().getRealPath(destLocation.toString());
        // 利用工具类生成唯一文件名称
        if (!file.isEmpty()) {
            String destFileName = CommonUtils.UUIDName(file.getOriginalFilename());
            File destFile = new File(path, destFileName);
            if (!destFile.getParentFile().exists()) {
                // 判断父类文件夹存不存在，不存在创建一个
                destFile.mkdirs();
            }
            try {
                file.transferTo(destFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            recollection.setRePhoto(destLocation.append("/").append(destFileName).toString());
        }
        if (reid != -1) {
           recollectionService.updateRecollection(recollection);
        } else {
            recollectionService.insertRecollection(recollection);
        }
        return "redirect:test.html";
    }
}
