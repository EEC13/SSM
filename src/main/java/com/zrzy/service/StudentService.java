package com.zrzy.service;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.zrzy.entity.Student;
import com.zrzy.dao.StudentDao;

public interface StudentService{
    int insert(Student student);
    int deleteByStuId(Long stuId);
    int updateByStuId(Student student);
    Student findById(Long id);
    /*根据姓名查询学生*/
    Student queryStudentByName(String  stuName);
	
}
