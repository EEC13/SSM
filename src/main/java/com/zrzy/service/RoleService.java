package com.zrzy.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import java.util.List;
import com.zrzy.entity.Role;


@Service
public interface RoleService{
    /*查*/
    List<Role> queryRole();
    /*增*/
    int insertRole(Role role);
    /*更*/
    int updateRole(@Param("ro") Role role);
    /*删*/
    int deleteRoleById(int id);

}
