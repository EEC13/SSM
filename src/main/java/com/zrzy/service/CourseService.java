package com.zrzy.service;

import com.zrzy.beans.PageBean;
import com.zrzy.entity.About;
import com.zrzy.entity.Course;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by XMH on 2017/3/10.
 */
public interface CourseService {
    /*插入一条课程信息*/
    int insert(Course course);

    /*根据id删除一条课程信息*/
    int deleteById(Long cId);

    /*根据id更新课程信息*/
    int update(Course course);

    /*根据课程体系csId查询课程信息*/
    List<Course> findByCsId(Integer csId);

    /**
     * 根据 ID 查询
     *
     * @param id ID
     * @return 实体对象
     */
    Course findById(Long id);

    /**
     * 分页查询(所有记录)当前页码的记录
     *
     * @param pageNumber  页码
     * @param pageSize    页面大小
     * @param courseSysId 课程体系 ID
     * @return 实体对象集合
     */
    List<Course> queryAsList(int pageNumber, int pageSize, int courseSysId);

    /**
     * 分页查询包装类
     *
     * @param pageNumber  页码
     * @param pageSize    页面大小
     * @param courseSysId 课程体系 ID
     * @return 实体对象集合
     */
    PageBean<Course> queryForPage(int pageNumber, int pageSize, int courseSysId);

    /**
     * 查询对应课程体系下课程数
     *
     * @return 记录条数
     */
    int getTotalSizeForCourseSys(int courseSysId);

    /**
     * 根据 ID 集合删除多条记录
     *
     * @param ids ID 集合
     */
    int deleteByIds(List<Long> ids);
}
