package com.zrzy.service;

import com.zrzy.beans.PageBean;
import com.zrzy.entity.About;

import java.util.List;


public interface AboutService {

    /*插入一条新闻*/
    int insert(About about);

    /*根据id删除一条新闻*/
    int deleteById(Long id);

    /*更新一条新闻*/
    int update(About about);

    /*根据分类查询新闻*/
    List<About> findByClassify(Integer classify);

    /**
     * 根据 ID 查询
     *
     * @param id ID
     * @return 实体对象
     */
    About findById(Long id);

    /**
     * 分页查询
     *
     * @param pageNumber 页码
     * @param pageSize   页面大小
     * @return 实体对象集合
     */
    List<About> queryAsList(int pageNumber, int pageSize);

    /**
     * 分页查询包装类
     *
     * @param pageNumber 页码
     * @param pageSize   页面大小
     * @return 分页包装类对象
     */
    PageBean<About> queryForPage(int pageNumber, int pageSize);

    /**
     * 根据集合 ID 删除多条记录
     *
     * @param aboutIds ID 集合
     */
    int deleteByIds(List<Long> aboutIds);

    /**
     * 获取指定分类的指定记录数的最新记录
     *
     * @param classify 分类
     * @param length   记录数
     * @return 对象集合
     */
    List<About> getForCondition(Integer classify, Integer length);
}
