package com.zrzy.service;

import com.zrzy.beans.PageBean;
import com.zrzy.entity.Student;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.zrzy.entity.Recollection;
import com.zrzy.dao.RecollectionDao;

public interface RecollectionService{
    /**
     * 分页查询
     *
     * @param pageNumber 页码
     * @param pageSize   页面大小
     * @return 实体对象集合
     */
    /*查询感言表所有信息*/
    List<Recollection> queryRecollectionByPage(int pageNumber, int pageSize);
    List<Recollection> queryRecollection();
    /**
     * 分页查询包装类
     *
     * @param pageNumber 页码
     * @param pageSize   页面大小
     * @return 分页包装类对象
     */
    PageBean<Recollection> queryForPage(int pageNumber, int pageSize);
    /*增加信息*/
    int insertRecollection(Recollection recollection/*, Student student*/);
    /*更改信息*/
    int updateRecollection(Recollection recollection);
    /*删除信息*/
    int deleteRecollectionById(Integer id);
    /*通过ID查询所有信息*/
    Recollection queryRecollectionById(Integer id);
    /*通过多个ID删除多条信息*/
    int deleteRecollectionByIds(String ids);

}
