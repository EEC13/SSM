package com.zrzy.service;


import java.util.List;

import com.zrzy.entity.Employment;
import com.zrzy.beans.PageBean;
import org.apache.ibatis.annotations.Param;

public interface EmploymentService {

    //增
    int insert(Employment employment);

    //更新
    int update(Employment employment);

    //查询
    Employment queryByPrimaryKey(Long empId);

    //删除
    int deleteByEmpId(Long empId);

    //根据id删除多个
    int deleteEmpByIds(String ids);

    //联表查询喜报
    List<Employment> queryGoodnews();

    //分页查询
    List<Employment> queryGoodnewsByPage(int pageNumber, int pageSize);

    //封装pageBeanss
    PageBean<Employment> queryForPage(int pageNumber, int pageSize);

    //根据课程体系id查询所有喜报
    List<Employment> queryAllGoodnews(@Param("str") String csname );
    /*根据年份，或者月份查询所有喜报*/
    List<Employment> queryAllGoodnewsByYear(@Param("year") String year);

}
