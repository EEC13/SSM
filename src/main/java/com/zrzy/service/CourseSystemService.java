package com.zrzy.service;

import com.zrzy.beans.PageBean;
import com.zrzy.entity.CourseSystem;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseSystemService{
    //查
    List<CourseSystem> queryCourseSystem();
    //增
    int insertCourseSystem(@Param("courseSystem") CourseSystem courseSystem);
    //更
    int updateCourseSystem(@Param("courseSystem") CourseSystem courseSystem);
    //删
    int deleteCourseSystemById(@Param("csId") Long id);
    /**
     * 分页查询
     *
     * @param pageNumber 页码
     * @param pageSize   页面大小
     * @return 实体对象集合
     */
   PageBean<CourseSystem> queryForPage(int pageNumber, int pageSize);

    /**
     * 分页查询
     *
     * @param pageNumber 页码
     * @param pageSize   页面大小
     * @return 实体对象集合
     */
    List<CourseSystem> queryAsList(int pageNumber, int pageSize) ;

    /**
     * 根据 ID 查询
     *
     * @param csId ID
     * @return 实体对象
     */
    CourseSystem findById(@Param("csId") Long csId);

    /**
     * 查询课程体系信息并携带课程信息
     *
     * @param courseId 课程体系 ID
     * @return 集合列表
     */
    List<CourseSystem> getCourseSystemWithCourse(Integer courseId);
}
