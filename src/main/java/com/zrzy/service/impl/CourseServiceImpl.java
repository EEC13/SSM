package com.zrzy.service.impl;

import com.zrzy.beans.PageBean;
import com.zrzy.dao.CourseDao;
import com.zrzy.entity.Course;
import com.zrzy.service.CourseService;
import com.zrzy.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by XMH on 2017/3/10.
 */
@Service
public class CourseServiceImpl implements CourseService {
    @Resource
    private CourseDao courseDao;

    @Override
    public int insert(Course course) {
        return courseDao.insert(course);
    }

    @Override
    public int deleteById(Long cId) {
        return courseDao.deleteById(cId);
    }

    @Override
    public int update(Course course) {
        return courseDao.update(course);
    }

    @Override
    public List<Course> findByCsId(Integer csId) {
        return courseDao.findByCsId(csId);
    }

    @Override
    public Course findById(Long id) {
        return courseDao.findById(id);
    }

    @Override
    public List<Course> queryAsList(int pageNumber, int pageSize, int courseSysId) {
        return courseDao.queryAsList((pageNumber - 1) * pageSize, pageSize, courseSysId);
    }

    @Override
    public PageBean<Course> queryForPage(int pageNumber, int pageSize, int courseSysId) {
        int totalSize = getTotalSizeForCourseSys(courseSysId);
        List<Course> entityList = queryAsList(pageNumber, pageSize,courseSysId);
        int totalPage = PageUtils.countTotalPage(totalSize, pageSize);
        return new PageBean<>(entityList, totalSize, totalPage, pageNumber, pageSize);
    }

    @Override
    public int getTotalSizeForCourseSys(int courseSysId) {
        return courseDao.getTotalSizeForCourseSys(courseSysId);
    }

    @Override
    public int deleteByIds(List<Long> ids) {
        return courseDao.deleteByIds(ids);
    }
}
