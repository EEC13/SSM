package com.zrzy.service.impl;

import com.zrzy.dao.StudentDao;
import com.zrzy.entity.Student;
import com.zrzy.service.StudentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lenovo on 2017/3/13.
 */
@Service
public class StudentServiceImpl implements StudentService {
    @Resource
    private StudentDao studentDao;

    @Override
    public int insert(Student student) {

        return  studentDao.insert(student);
    }

    @Override
    public int deleteByStuId(Long stuId) {
        return 0;
    }

    @Override
    public int updateByStuId(Student student) {
        return 0;
    }

    @Override
    public Student findById(Long id) {
        return null;
    }

    @Override
    public Student queryStudentByName(String stuName) {
        return  studentDao.queryStudentByName(stuName);
    }
}
