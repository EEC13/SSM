package com.zrzy.service.impl;

import com.zrzy.beans.PageBean;
import com.zrzy.dao.AboutDao;
import com.zrzy.entity.About;
import com.zrzy.service.AboutService;
import com.zrzy.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by XMH on 2017/3/10.
 */
@Service
public class AboutServiceImpl implements AboutService {
    @Resource
    private AboutDao aboutDao;

    @Override
    public int insert(About about) {
        return aboutDao.insert(about);
    }

    @Override
    public int deleteById(Long id) {
        return aboutDao.deleteById(id);
    }

    @Override
    public int update(About about) {
        return aboutDao.updateById(about);
    }

    @Override
    public List<About> findByClassify(Integer classify) {
        return aboutDao.findByClassify(classify);
    }

    @Override
    public About findById(Long id) {
        return aboutDao.findById(id);
    }

    @Override
    public List<About> queryAsList(int pageNumber, int pageSize) {
        return aboutDao.queryAsList((pageNumber - 1) * pageSize, pageSize);
    }

    @Override
    public PageBean<About> queryForPage(int pageNumber, int pageSize) {
        int totalSize = aboutDao.getTotalSize();
        List<About> entityList = queryAsList(pageNumber, pageSize);
        int totalPage = PageUtils.countTotalPage(totalSize, pageSize);
        return new PageBean<>(entityList, totalSize, totalPage, pageNumber, pageSize);
    }

    @Override
    public int deleteByIds(List<Long> aboutIds) {
        return aboutDao.deleteByIds(aboutIds);
    }

    @Override
    public List<About> getForCondition(Integer classify, Integer length) {
        return aboutDao.getForCondition(classify, length);
    }
}
