package com.zrzy.service.impl;

import com.zrzy.beans.PageBean;
import com.zrzy.dao.CourseSystemDao;
import com.zrzy.entity.CourseSystem;
import com.zrzy.service.CourseSystemService;
import com.zrzy.utils.PageUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by gtz on 2017/3/13.
 */
@Service
public class CourseSystemServiceImp implements CourseSystemService{
    @Resource
    private CourseSystemDao courseSystemDao;

    @Override
    public List<CourseSystem> queryCourseSystem() {
        return courseSystemDao.queryCourseSystem();
    }

    @Override
    public int insertCourseSystem(@Param("courseSystem") CourseSystem courseSystem) {
        return courseSystemDao.insertCourseSystem(courseSystem);
    }

    @Override
    public int updateCourseSystem(@Param("courseSystem") CourseSystem courseSystem) {
        return courseSystemDao.updateCourseSystem(courseSystem);
    }

    @Override
    public int deleteCourseSystemById(@Param("csId") Long id) {
        return courseSystemDao.deleteCourseSystemById(id);
    }

    @Override
    public PageBean<CourseSystem> queryForPage(int pageNumber, int pageSize) {
        int totalSize = courseSystemDao.getTotalSize();
        List<CourseSystem> entityList = queryAsList(pageNumber, pageSize);
        int totalPage = PageUtils.countTotalPage(totalSize, pageSize);
        return new PageBean<>(entityList, totalSize, totalPage, pageNumber, pageSize);
    }

    @Override
    public List<CourseSystem> queryAsList(int pageNumber, int pageSize) {
        return courseSystemDao.queryAsList((pageNumber - 1) * pageSize, pageSize);
    }

    @Override
    public CourseSystem findById(@Param("csId") Long csId) {
        return courseSystemDao.findById(csId);

    }

    @Override
    public List<CourseSystem> getCourseSystemWithCourse(Integer courseId) {
        return courseSystemDao.getCourseSystemWithCourse(courseId);
    }


}
