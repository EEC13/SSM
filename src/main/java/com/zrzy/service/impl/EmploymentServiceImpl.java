package com.zrzy.service.impl;

import com.zrzy.beans.PageBean;
import com.zrzy.dao.EmploymentDao;
import com.zrzy.entity.Employment;
import com.zrzy.service.EmploymentService;
import com.zrzy.utils.PageUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * Created by XMH on 2017/3/12.
 */
@Service
public class EmploymentServiceImpl implements EmploymentService {
    @Resource
    private EmploymentDao employmentDao;

    @Override
    public List<Employment> queryAllGoodnewsByYear(@Param("year") String year) {
        return employmentDao.queryAllGoodnewsByYear(year);
    }

    @Override
    public List<Employment> queryAllGoodnews(@Param("str") String csname) {
        return employmentDao.queryAllGoodnews(csname);
    }

    @Override
    public int insert(Employment employment) {
        return employmentDao.insert(employment);
    }

    @Override
    public int update(Employment employment) {

        return employmentDao.update(employment);
    }

    @Override
    public Employment queryByPrimaryKey(Long empId) {

        return employmentDao.queryByPrimaryKey(empId);
    }

    @Override
    public int deleteByEmpId(Long empId) {
        return employmentDao.deleteByEmpId(empId);
    }

    @Override
    public int deleteEmpByIds(String ids) {
        return employmentDao.deleteEmpByIds(ids);
    }

    @Override
    public List<Employment> queryGoodnews() {
        return employmentDao.queryGoodnews();
    }

    @Override
    public List<Employment> queryGoodnewsByPage(int pageNumber, int pageSize) {

        return employmentDao.queryGoodnewsByPage((pageNumber - 1) * pageSize, pageSize);
    }

    @Override
    public PageBean<Employment> queryForPage(int pageNumber, int pageSize) {
        int totalSize = queryGoodnews().size();
        List<Employment> entityList = queryGoodnewsByPage(pageNumber, pageSize);
        int totalPage = PageUtils.countTotalPage(totalSize, pageSize);
        return new PageBean<>(entityList, totalSize, totalPage, pageNumber, pageSize);
    }

}
