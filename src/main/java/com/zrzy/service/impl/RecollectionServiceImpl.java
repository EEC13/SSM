package com.zrzy.service.impl;

import com.zrzy.beans.PageBean;
import com.zrzy.dao.RecollectionDao;
import com.zrzy.entity.Recollection;
import com.zrzy.entity.Student;
import com.zrzy.service.RecollectionService;
import com.zrzy.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lenovo on 2017/3/12.
 */
@Service
public class RecollectionServiceImpl implements RecollectionService{
    @Resource
    private RecollectionDao recollectionDao;
    @Override
    public List<Recollection> queryRecollection() {
        return recollectionDao.queryRecollection();
    }

    @Override
    public List<Recollection> queryRecollectionByPage(int pageNumber,int pageSize) {
        return recollectionDao.queryRecollectionByPage((pageNumber - 1) * pageSize, pageSize);
    }
    /*通过多个ID删除多条信息*/
    @Override
    public int deleteRecollectionByIds(String ids) {
        return recollectionDao.deleteRecollectionByIds(ids);
    }

    @Override
    public PageBean<Recollection> queryForPage(int pageNumber, int pageSize) {
       int totalSize = recollectionDao.getTotalSize();
       List<Recollection> recollectionList=queryRecollectionByPage(pageNumber, pageSize);
        int totalPage= PageUtils.countTotalPage(totalSize, pageSize);
        return new PageBean<>(recollectionList, totalSize, totalPage, pageNumber, pageSize);
    }

    @Override
    public int insertRecollection(Recollection recollection/*,Student student*/) {
        return recollectionDao.insertRecollection(recollection/*,student*/);
    }

    @Override
    public int updateRecollection(Recollection recollection) {

        return recollectionDao.updateRecollection(recollection);
    }

    @Override
    public int deleteRecollectionById(Integer id) {
        return recollectionDao.deleteRecollectionById(id);
    }

    @Override
    public Recollection queryRecollectionById(Integer id) {
        return recollectionDao.queryRecollectionById(id);
    }
}
