package com.zrzy.service.impl;

import com.zrzy.beans.PageBean;
import com.zrzy.dao.TeacherDao;
import com.zrzy.entity.Teacher;
import com.zrzy.service.TeacherService;
import com.zrzy.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lenovo on 2017/3/14.
 */
@Service
public class TeacherServiceImpl implements TeacherService{
    @Resource
    private TeacherDao teacherDao;
    @Override
    public List<Teacher> queryTeacherByPage(int pageNumber, int pageSize) {
        return teacherDao.queryTeacherByPage((pageNumber - 1) * pageSize, pageSize);
    }
    public PageBean<Teacher> queryForPage(int pageNumber, int pageSize) {
        List<Teacher> teacherList=queryTeacherByPage(pageNumber,pageSize);
        int totalSize=teacherDao.getTotalSize();
        int totalPage= PageUtils.countTotalPage(totalSize, pageSize);
        return new PageBean<>(teacherList, totalSize, totalPage, pageNumber, pageSize);
    }
    @Override
    public int insertTeacher(Teacher teacher) {

        return teacherDao.insertTeacher(teacher);
    }

    @Override
    public int updateTeacher(Teacher teacher) {

        return teacherDao.updateTeacher(teacher);
    }

    @Override
    public int deleteAllTeacher(String ids) {

        return teacherDao.deleteAllTeacher(ids);
    }

    @Override
    public int deleteTeacher(Integer id) {
        return teacherDao.deleteTeacher(id);
    }

    /*@Override
    public int getTotalSize() {
        return teacherDao.getTotalSize();
    }*/

    @Override
    public Teacher queryTeacherById(Integer id) {

        return teacherDao.queryTeacherById(id);
    }
}
