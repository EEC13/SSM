package com.zrzy.service;

import com.zrzy.beans.PageBean;
import com.zrzy.entity.Recollection;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.zrzy.entity.Teacher;
import com.zrzy.dao.TeacherDao;
public interface TeacherService{

    List<Teacher> queryTeacherByPage(int pageNumber, int pageSize);
    PageBean<Teacher> queryForPage(int pageNumber, int pageSize);
    /*插入*/
    int insertTeacher(Teacher teacher);
    /*更新*/
    int updateTeacher(Teacher teacher);
    /*删除*/
    int deleteTeacher(Integer id);
    /*删除多个*/
    int deleteAllTeacher(String ids);
    /*通过ID查询*/
    Teacher queryTeacherById(Integer id);


}
